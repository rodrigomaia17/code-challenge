# Code Challenge

The final code is located in tenfold-code-challenge.js. I've use a template parser to generate the html called pug. 

## How to run tests

- Install dependencies
`yarn`
or
`npm install`

- Run!
`yarn test`
or
`npm test`
