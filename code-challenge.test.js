const arrayDiff = require('./tenfold-code-challenge.js').arrayDiffToHtmlTable;

test('empty array returns empty string', () => {
  const result = arrayDiff([],[]);
  const expected = '<table><theader><tr></tr></theader><tbody></tbody></table>';
  expect(result).toBe(expected);
});


test('one array with one object should return simple structure', () => {
  const result = arrayDiff([],[{ _id: 1 }]);

  const expected = '<table><theader><tr><th>_id</th></tr></theader><tbody><tr><td><b>1</b></td></tr></tbody></table>';

  expect(result).toBe(expected);
});

test('one array with one object with multiple keys should return simple structure', () => {
  const result = arrayDiff([],[{ _id: 1, someKey: true }]);

  const expected = '<table><theader><tr><th>_id</th><th>someKey</th></tr></theader><tbody><tr><td><b>1</b></td><td><b>true</b></td></tr></tbody></table>';

  expect(result).toBe(expected);
});

test('one array with multiple objects with single key should return simple structure', () => {
  const result = arrayDiff([],[{ _id: 1 }, {_id: 2 }]);

  const expected = '<table><theader><tr><th>_id</th></tr></theader><tbody><tr><td><b>1</b></td></tr><tr><td><b>2</b></td></tr></tbody></table>';

  expect(result).toBe(expected);
});

test('one array with one deep object should create html ', () => {
  const result = arrayDiff([],[{ _id: 1, meta: { subKey1: 1234 } }]);

  const expected = '<table><theader><tr><th>_id</th><th>meta_subKey1</th></tr></theader><tbody><tr><td><b>1</b></td><td><b>1234</b></td></tr></tbody></table>';

  expect(result).toBe(expected);
});


test('should make differences bold', () => {
  const result = arrayDiff([{ _id: 1, someKey: 1, someKey2: 2 }],[{ _id: 1, someKey: 2, someKey2: 2 }]);

  const expected = '<table><theader><tr><th>_id</th><th>someKey</th><th>someKey2</th></tr></theader><tbody><tr><td>1</td><td><b>2</b></td><td>2</td></tr></tbody></table>';

  expect(result).toBe(expected);
});

test('test given example', () => {
  var prevArray = [ {_id:1, someKey: "RINGING", meta: { subKey1: 1234, subKey2: 52 } } ];
  var currArray = [ {_id:1, someKey: "HANGUP",  meta: { subKey1: 1234 } },{_id:2, someKey: "RINGING", meta: { subKey1: 5678, subKey2: 207, subKey3: 52 } } ];

  const result = arrayDiff(prevArray, currArray);

  expect(result).toBe('<table><theader><tr><th>_id</th><th>someKey</th><th>meta_subKey1</th><th>meta_subKey2</th><th>meta_subKey3</th></tr></theader><tbody><tr><td>1</td><td><b>HANGUP</b></td><td>1234</td><td><b>DELETED</b></td><td></td></tr><tr><td><b>2</b></td><td><b>RINGING</b></td><td><b>5678</b></td><td><b>207</b></td><td><b>52</b></td></tr></tbody></table>');
});

